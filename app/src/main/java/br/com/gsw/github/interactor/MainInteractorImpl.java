package br.com.gsw.github.interactor;

import java.util.List;

import br.com.gsw.github.entity.GithubRepository;
import br.com.gsw.github.repository.GithubRepoRepository;
import br.com.gsw.github.repository.RepositoryCallback;

public class MainInteractorImpl implements MainInteractor {

    private GithubRepoRepository githubRepoRepository;


    public MainInteractorImpl() {
        githubRepoRepository = new GithubRepoRepository();
    }


    @Override
    public void loadRepositories(int since, final LoadRepositoriesCallback loadRepositoriesCallback) {
        githubRepoRepository.loadRepositories(since, new RepositoryCallback<List<GithubRepository>>() {

            @Override
            public void onSuccess(List<GithubRepository> o) {
                loadRepositoriesCallback.onSuccessLoadRepositories(o);
            }

            @Override
            public void onFailure(String errorMessage) {
                loadRepositoriesCallback.onFailureLoadRepositories(errorMessage);
            }
        });
    }
}
