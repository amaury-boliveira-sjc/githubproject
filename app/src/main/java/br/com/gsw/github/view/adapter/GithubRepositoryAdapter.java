package br.com.gsw.github.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import br.com.gsw.github.R;
import br.com.gsw.github.entity.GithubRepository;
import br.com.gsw.github.router.InternetRouter;
import br.com.gsw.github.view.MainActivity;
import br.com.gsw.github.view.MainView;

public class GithubRepositoryAdapter extends RecyclerView.Adapter<GithubRepositoryAdapter.GithubRepositoryViewHolder> {


    private Context context;
    private List<GithubRepository> githubRepositoryList;
    private MainView mainView;

    public GithubRepositoryAdapter(Context context, List<GithubRepository> githubRepositoryList) {
        this.context = context;
        this.githubRepositoryList = githubRepositoryList;
        this.mainView = (MainView) context;
    }

    @NonNull
    @Override
    public GithubRepositoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_git, parent, false);
        GithubRepositoryViewHolder githubRepositoryViewHolder = new GithubRepositoryViewHolder(view);
        return githubRepositoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GithubRepositoryViewHolder holder, int position) {
        GithubRepository githubRepository = githubRepositoryList.get(position);
        holder.initData(githubRepository);
    }

    @Override
    public int getItemCount() {
        return githubRepositoryList.size();
    }

    class GithubRepositoryViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewAvatar;
        private TextView textviewTitle;
        private TextView textViewDescription;

        public GithubRepositoryViewHolder(View itemView) {
            super(itemView);

            initComponents();
            initListeners();

        }

        public void initData(GithubRepository githubRepository) {
            textviewTitle.setText(githubRepository.getName());
            textViewDescription.setText(githubRepository.getDescription());
            Glide.with(context).load(githubRepository.getOwner().getAvatar()).into(imageViewAvatar);
        }

        private void initListeners() {
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                   GithubRepository gitRepository  = githubRepositoryList.get(getAdapterPosition());
                   mainView.showUrl(gitRepository.getUrl());
                }
            });
        }

        private void initComponents() {
            imageViewAvatar = itemView.findViewById(R.id.imageview_avatar);
            textviewTitle = itemView.findViewById(R.id.textview_title);
            textViewDescription = itemView.findViewById(R.id.textview_description);
        }



    }

}
