package br.com.gsw.github.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import br.com.gsw.github.api.GithubRepoAPI;
import br.com.gsw.github.entity.GithubRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GithubRepoRepository {

    GithubRepoAPI githubRepoAPI;

    public GithubRepoRepository() {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.github.com/").addConverterFactory(GsonConverterFactory.create(gson)).build();
        githubRepoAPI = retrofit.create(GithubRepoAPI.class);
    }

    public void loadRepositories(int since, final RepositoryCallback<List<GithubRepository>> repositoryCallback){
        githubRepoAPI.getRepositories(since).enqueue(new Callback<List<GithubRepository>>() {
            @Override
            public void onResponse(Call<List<GithubRepository>> call, Response<List<GithubRepository>> response) {
                if (response.isSuccessful()){
                    repositoryCallback.onSuccess(response.body());
                }else{
                    repositoryCallback.onFailure("Erro ao acessar repositórios");
                }
            }

            @Override
            public void onFailure(Call<List<GithubRepository>> call, Throwable t) {
                repositoryCallback.onFailure("Erro ao acessar repositórios");
            }
        });
    }
}
