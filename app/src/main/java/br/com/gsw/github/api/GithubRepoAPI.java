package br.com.gsw.github.api;

import java.util.List;

import br.com.gsw.github.entity.GithubRepository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithubRepoAPI {

    @GET("repositories")
    Call<List<GithubRepository>> getRepositories(@Query("since") int since);


}
