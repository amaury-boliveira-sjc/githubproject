package br.com.gsw.github.router;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;


public class InternetRouter {

    public void callInternet(Activity activity, String url){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        activity.startActivity(intent);
    }


}
