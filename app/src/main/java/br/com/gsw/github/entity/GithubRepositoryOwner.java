package br.com.gsw.github.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GithubRepositoryOwner  implements Serializable{

    @SerializedName("avatar_url")
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
