package br.com.gsw.github.presenter;

public interface MainPresenter {

    void loadNewRepositories(int since);
}
