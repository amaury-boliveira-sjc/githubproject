package br.com.gsw.github.interactor;

import java.util.List;

import br.com.gsw.github.entity.GithubRepository;

public interface MainInteractor {

    public interface LoadRepositoriesCallback{

        void onSuccessLoadRepositories(List<GithubRepository> githubRepositoryList);

        void onFailureLoadRepositories(String errorMessage);
    }

    void loadRepositories(int since, LoadRepositoriesCallback loadRepositoriesCallback);
}
