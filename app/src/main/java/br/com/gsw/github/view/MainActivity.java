package br.com.gsw.github.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import br.com.gsw.github.R;
import br.com.gsw.github.entity.GithubRepository;
import br.com.gsw.github.entity.GithubRepositoryOwner;
import br.com.gsw.github.presenter.MainPresenter;
import br.com.gsw.github.presenter.MainPresenterImpl;
import br.com.gsw.github.router.InternetRouter;
import br.com.gsw.github.view.adapter.GithubRepositoryAdapter;

public class MainActivity  extends AppCompatActivity implements MainView{

    private RecyclerView recyclerView;
    private GithubRepositoryAdapter githubRepositoryAdapter;
    private List<GithubRepository> githubRepositories;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        initListeners();
        initData();
    }

    private void initData() {
        githubRepositories = new ArrayList<>();
        githubRepositoryAdapter = new GithubRepositoryAdapter(this, githubRepositories);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(githubRepositoryAdapter);
        mainPresenter = new MainPresenterImpl(this);
        mainPresenter.loadNewRepositories(0);
    }

    private void initListeners() {

    }

    private void initComponents() {
        recyclerView = findViewById(R.id.recycle_git_list);
    }


    @Override
    public void showListRepositories(List<GithubRepository> githubRepositoryList) {
        githubRepositories.addAll(githubRepositoryList);
        githubRepositoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showUrl(String url) {
        InternetRouter internetRouter = new InternetRouter();
        internetRouter.callInternet(this, url);
    }
}
