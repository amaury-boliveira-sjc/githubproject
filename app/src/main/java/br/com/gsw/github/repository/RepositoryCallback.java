package br.com.gsw.github.repository;

public interface RepositoryCallback<O> {

    void onSuccess(O o);

    void onFailure(String errorMessage);
}
