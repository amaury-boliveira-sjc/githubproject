package br.com.gsw.github.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GithubRepository implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("private")
    private boolean priv;
    @SerializedName("html_url")
    private String url;
    @SerializedName("description")
    private String description;
    @SerializedName("owner")
    private GithubRepositoryOwner owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPriv() {
        return priv;
    }

    public void setPriv(boolean priv) {
        this.priv = priv;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GithubRepositoryOwner getOwner() {
        return owner;
    }

    public void setOwner(GithubRepositoryOwner owner) {
        this.owner = owner;
    }
}
