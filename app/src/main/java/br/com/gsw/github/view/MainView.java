package br.com.gsw.github.view;

import java.util.List;

import br.com.gsw.github.entity.GithubRepository;

public interface MainView {

    void showListRepositories(List<GithubRepository> githubRepositoryList);

    void showErrorMessage(String errorMessage);

    void showUrl(String url);
}
