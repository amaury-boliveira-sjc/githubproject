package br.com.gsw.github.presenter;

import java.util.List;

import br.com.gsw.github.entity.GithubRepository;
import br.com.gsw.github.interactor.MainInteractor;
import br.com.gsw.github.interactor.MainInteractorImpl;
import br.com.gsw.github.view.MainView;

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;
    private MainInteractor mainInteractor;

    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
        mainInteractor = new MainInteractorImpl();
    }

    @Override
    public void loadNewRepositories(int since) {
        mainInteractor.loadRepositories(since, new MainInteractor.LoadRepositoriesCallback() {
            @Override
            public void onSuccessLoadRepositories(List<GithubRepository> githubRepositoryList) {
                mainView.showListRepositories(githubRepositoryList);
            }

            @Override
            public void onFailureLoadRepositories(String errorMessage) {
                mainView.showErrorMessage(errorMessage);
            }
        });
    }
}
